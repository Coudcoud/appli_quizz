<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * role
 *
 * @ORM\Table(name="role")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\roleRepository")
 */
class role
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="adminitrateur", type="string", length=255)
     */
    private $adminitrateur;

    /**
     * @var int
     *
     * @ORM\Column(name="utilisateur_id", type="integer")
     */
    private $utilisateurId;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set adminitrateur
     *
     * @param string $adminitrateur
     *
     * @return role
     */
    public function setAdminitrateur($adminitrateur)
    {
        $this->adminitrateur = $adminitrateur;

        return $this;
    }

    /**
     * Get adminitrateur
     *
     * @return string
     */
    public function getAdminitrateur()
    {
        return $this->adminitrateur;
    }

    /**
     * Set utilisateurId
     *
     * @param integer $utilisateurId
     *
     * @return role
     */
    public function setUtilisateurId($utilisateurId)
    {
        $this->utilisateurId = $utilisateurId;

        return $this;
    }

    /**
     * Get utilisateurId
     *
     * @return int
     */
    public function getUtilisateurId()
    {
        return $this->utilisateurId;
    }
}

